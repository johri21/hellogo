package main

import "fmt"

func main() {
	defer fmt.Println("After the function call");
	fmt.Println("function call");
}
