package visibility

func Public() string {
	return "I am reachable !"
}

func private() string {
	return "I am not reachable !"
}
