package variables

import "fmt"

var i int
var N = "zomato"

func Variables() {
	i = 0
	var j = 0
	k := 0
	var (
		l = ""; m = 0
	)
	fmt.Println(i, j, k, l, m, N)
}
