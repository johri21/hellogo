package types

import "fmt"

var array [5]int

var slice = make([]string, 0, 5)

var dict = map[string]string{
	"name":"zomato",
	"age":"8",
}

func ExpandSlice() {
	fmt.Println(array)
	fmt.Println(slice)
	slice = append(slice, "lol")
	fmt.Println(slice)
	fmt.Println(len(array), len(slice), cap(array), cap(slice))
}

func GetMapElement() {
	value, ok := dict["name"]
	if ok {
		fmt.Println(value)
	}

	//with limited scope
	if value1, ok1 := dict["name"]; ok1 {
		fmt.Println(value1)
	}

	//with optional value
	if _, ok2 := dict["stack"]; !ok2 {
		fmt.Println("not found")
	}
}
