package functions

func add(a int, b int) int {
	return a + b
}

func swap(a int, b int) (int, int) {
	return b, a
}

func compute(a int, b int) (sum int, diff int) {
	sum = a + b
	diff = a - b
	return
}
