package main


import (
	"fmt"
	"math"
)

func compute(a float64,b float64 ,fn func(float64,float64) float64) float64 {
	return fn(a,b)
}


func main() {
	
	cal := func(a, b int) int {
		return a*a + b*b
	} 
	fmt.Println(cal(3, 4))
	fmt.Println(compute(3.0, 4.0, math.Max))
	fmt.Println(compute(3.0, 4.0, math.Min))	
}
