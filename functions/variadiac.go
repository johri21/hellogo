package main

import "fmt"

func PrintLanguageNames(names ...string) {
	fmt.Println(names)
}
func RunPrintLanguageNames() {
	PrintLanguageNames("golang")
	PrintLanguageNames("python", "java")
}

func main() {
	RunPrintLanguageNames()
}
